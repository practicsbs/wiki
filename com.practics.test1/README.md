# Module Name

This guide is based on the version X.X.XXX of the module.

# Table of Contents
  1. [Introduction](#introduction)
  2. [Configuration](#configuration)
    1. [Preferences](#preferences)
    2. [Hooks](#hooks)
    3. [Other config...](#other-config)
  3. [Dependencies](#dependencies)
  4. [Authors](#authors)
  5. [License](#license)

## Introduction

This module allows to manage a Gift List from the WebPOS, as well as sales, payments, diferent kind of lists, geenration of gift cards, returns...

## Configuration

In order to have this module up and running correctly, a basic configuration must be done. In the following points is explained how to configure each part of the module.

### Preferences

| Name | Value | Description | Default | Available From |
| :--: | :---: | :---------: | :-----: | :------------: |
| Preference Name 1 | PBS_Preference1 | Enables an new functionaltiy | Disabled | X.X.XXX |
| Preference Name 2 | PBS_Preference2 | Does an action by default | Enabled | X.X.XXX |


### Hooks

List of hooks available

| Name | Executed | Available From | Arguments |
| :--: | :------: | :------------: | :-------: |
| PBS_ExampleHook | Executed after an action has been made. This hook is intended to add the logic after this actions | X.X.XXX | **line:** The line where the action is executed. **oldStatus:** The line status before being updated. **newStatus:** The line status after being updated |

### Other config

Other configuration, like creating new config registers...

## Dependencies

* [Example Module Practics](https://practicsbs.com/example-module) - 1.0.300
* [Openbravo for Retail](http://centralrepository.openbravo.com/openbravo/org.openbravo.forge.ui/ForgeModuleDetail/Openbravo-For-Retail) - 1.8.4100 - RR19Q1


## Authors

* **Juanjo Almendro** - *Initial work* - [juanjo.almendro@practicsbs.com](mailto:juanjo.almendro@practicsbs.com)

## License

Licensed under the Openbravo Commercial License version 1.0. You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html or in the legal folder of this module distribution.
