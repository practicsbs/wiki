# Pracitcs Forge - Modulo Server

Esta guía está basada en la versión 1.0.0 de este modulo

# Tabla de Contenido
  1. [Introducción](#introduction)
  2. [Configuración](#configuration)
    1. [Bitbucket](#bitbucket)
    2. [Proceso Background](#background)
  3. [Guia Tecnica](#technical-guide)
    1. [URL's](#add-new-documents)
  4. [Sobre](#about)
    1. [Dependencias](#dependencies)
    2. [Autores](#authors)
    3. [Licencia](#license)

## Introducción

In this document you will find all the technicals details and some HOW-TO's, as well as details on how to configure the module.

## Configuración

In order to have this module up and running correctly, a basic configuration must be done. In the following points is explained how to configure each part of the module.

### Bitbucket

Hay que configurar la conexión contra BitBucket en la ventana Configuración Forge. Hay que indicar un usuario (con acceso a todo si puede ser), el password y el grupo (que será practicsbs).

### Proceso Background

Hay que programar el proceso background _**Module Update Process**_ para que se ejecute cada X minutos.

Ojo! La primera ejecución tarda unos 30 minutos, ejecutarla en modo *Ejecutar Immediatamente*. Despues solo se bajara los cambios desde la ultima ejecución y deberia ser bastante rapido, así que se podria programar para que salte cada 10 minutos o así.

## Guia Tecnica

### URL's

Estas son las url's que se pueden utilizar para obtener la info desde la forge, siempre seran peticiones GET

#### Obtener listado de modulos

**URL:** https://server/openbravo/PracticsForge/

Esto nos devuelve un array con todos los modulos junto sus versiones

| Parametro | Obligatorio | Descripción |
| :-------: | :---------: | :---------: |
| query | No | Se utiliza para filtrar los resultados devueltos por nombre del modulo

**Por ejemplo:**  https://server/openbravo/PracticsForge/?query=gafa+template

```JSON
[
    {
        "_identifier": "Comercial Gafa Template",
        "_entityName": "prfo_module",
        "$ref": "prfo_module/3DD522F1D5B24629BE0EE27E0447A2C5",
        "id": "3DD522F1D5B24629BE0EE27E0447A2C5",
        "client": "78B9CF539F004681913322D3E0789604",
        "client$_identifier": "Pollyanna",
        "organization": "0",
        "organization$_identifier": "*",
        "active": true,
        "creationDate": "2019-12-29T16:54:41+01:00",
        "createdBy": "100",
        "createdBy$_identifier": "Openbravo",
        "updated": "2019-12-29T16:54:41+01:00",
        "updatedBy": "100",
        "updatedBy$_identifier": "Openbravo",
        "name": "Comercial Gafa Template",
        "module": "09C040B152F748B8B7C9D8D331B5665D",
        "javaPackage": "es.comercialgafa.template",
        "slug": "es.comercialgafa.template",
        "recordTime": 1577646249474,
        "versions": [
            "1.0.123",
            "1.0.104"
        ]
    }
]
```
#### Obtener OBX de un modulo

**URL:** https://server/openbravo/PracticsForge/

Esto nos devuelve un fichero con el OBX del modulo

| Parametro | Obligatorio | Descripción |
| :-------: | :---------: | :---------: |
| moduleId | Si | ID del modulo en la Forge, no confundir con el ad_module_id |
| version | Si | Version del modulo a descargar |

**Por ejemplo:**  https://server/openbravo/PracticsForge/?moduleId=3DD522F1D5B24629BE0EE27E0447A2C5&version=1.0.123

## Sobre el módulo

### Dependencias

* [Openbravo 3.0](http://centralrepository.openbravo.com/openbravo/org.openbravo.forge.ui/ForgeModuleDetail/Mobile-Core-Infrastructurehttp://wiki.openbravo.com/wiki/Release_Notes/3.0PR19Q3.2) - 3.0.36241 - 3.0PR19Q3.2

### Autores

* **Juanjo Almendro** - *Trabajo Inicial* - [juanjo.almendro@practicsbs.com](mailto:juanjo.almendro@practicsbs.com)

### Licencia

Licensed under the Openbravo Commercial License version 1.0. You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html or in the legal folder of this module distribution.
